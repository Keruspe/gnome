# Copyright 2008 Alexander Færøy <ahf@exherbo.org>
# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ]

SUMMARY="GTK API documentation generator"
HOMEPAGE="http://www.gtk.org/${PN}"

UPSTEAM_DOCUMENTATION="
    https://developer.gnome.org/gtk-doc-manual/stable/ [[
        description = [ User manual ]
    ]]
"

LICENCES="FDL-1.1 GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    source-highlight [[ description = [ Support source highlighting using source-highlight ] ]]
    vim [[ description = [ Support source highlighting using vim ] ]]

    ( source-highlight vim ) [[ number-selected = at-most-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        app-text/docbook-xml-dtd:4.3
        app-text/docbook-xsl-stylesheets
        dev-libs/libxslt
        dev-lang/perl:=[>=5.18.0]
        dev-lang/python:=[>=2.3]
        gnome-desktop/gnome-doc-utils
        gnome-desktop/yelp-tools
        source-highlight? ( app-text/source-highlight )
        vim? ( app-editors/vim )
    run:
        app-doc/gtk-doc-autotools[~${PV}]
    test:
        dev-libs/glib:2[>=2.6]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-depscan-fixxref-rebase-Don-t-hardcode-pkg-config-bin.patch
)
DEFAULT_SRC_TEST_PARAMS=( -j1 )

src_configure() {
    local highlighter="no"

    option source-highlight && highlighter="source-highlight"
    option vim && highlighter="vim"

    econf \
        "--with-highlight=${highlighter}"
}

src_install() {
    default

    # These are now provided by the gtk-doc-autotools package
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/{gtkdoc-rebase,gtkdocize} \
           "${IMAGE}"usr/share/aclocal/gtk-doc.m4      \
           "${IMAGE}"usr/share/gtk-doc/data/gtk-doc{,.flat,.notmpl,.notmpl-flat}.make
    edo rmdir "${IMAGE}"usr/share/aclocal
}

