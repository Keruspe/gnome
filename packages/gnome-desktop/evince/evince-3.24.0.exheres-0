# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require evince

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection gtk-doc
    djvu       [[ description = [ Enable support for DjVu format ] ]]
    dvi        [[ description = [ Enable support for DVI format ] ]]
    gstreamer  [[ description = [ Enable support for showing videos ] ]]
    keyring    [[ description = [ Use GNOME Keyring to store passwords ] ]]
    nautilus   [[ description = [ Build Nautilus extensions ] ]]
    postscript [[ description = [ Enable support for PostScript format ] ]]
    tiff       [[ description = [ Enable support for multi-page TIFF format ] ]]
    xps        [[ description = [ enable support for XPS documents ] ]]
    ( linguas: af an ar as ast be be@latin bg bn bn_IN br ca ca@valencia cs cy da de dz el en_CA
               en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hu id it ja ka kk km kn ko ks ku
               lt lv mai mg mk ml mn mr ms my nb nds ne nl nn oc or pa pl ps pt pt_BR ro ru rw si
               sk sl sq sr sr@latin sv ta te tg th tr ug uk vi wa zh_CN zh_HK zh_TW zu )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        dev-util/intltool[>=0.35]
        virtual/pkg-config[>=0.20]
        dev-libs/libxml2:2.0 [[ note = [ for xmllint ] ]]
        gnome-desktop/yelp-tools
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        x11-libs/gtk+:3[>=3.20.0][gobject-introspection?]
        dev-libs/glib:2[>=2.36.0]
        dev-libs/libxml2:2.0[>=2.5.0]
        gnome-desktop/adwaita-icon-theme[>=2.17.1]
        gnome-desktop/gnome-desktop:3.0
        x11-libs/cairo[>=1.10.0]
        app-text/poppler[>=0.33.0][cairo][glib]
        dvi? (
            app-text/libspectre[>=0.2.0]
            dev-libs/kpathsea
            media-libs/t1lib
        )
        djvu? ( app-text/djvu[>=3.5.22] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.0] )
        gstreamer? ( media-libs/gstreamer:1.0 )
        keyring? ( dev-libs/libsecret:1[>=0.5] )
        nautilus?  (
            gnome-desktop/nautilus[>=2.91.4]
        )
        postscript? ( app-text/libspectre[>=0.2.0] )
        tiff? ( media-libs/tiff[>=3.6] )
        xps? ( dev-libs/libgxps[>=0.2.1] )
    run:
        x11-misc/shared-mime-info
    suggestion:
        gnome-desktop/gvfs [[ description = [ Required for bookmark functionality ] ]]
"

RESTRICT="test" # requires X and dogtail

DEFAULT_SRC_CONFIGURE_PARAMS=( '--enable-pdf' '--enable-comics' '--enable-libgnome-desktop' )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( keyring )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'djvu' 'dvi' 'gobject-introspection introspection' 'gstreamer multimedia'
                                       'gtk-doc' 'nautilus' 'postscript ps' 'tiff' 'xps' )

