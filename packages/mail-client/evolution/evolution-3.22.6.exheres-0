# Copyright 2009, 2011, 2013 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require evolution gnome.org [ suffix="tar.xz" ] gsettings gtk-icon-cache freedesktop-desktop

PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    bogofilter gtk-doc help libnotify weather

    bogofilter   [[ description = [ Enable spam filtering using Bogofilter ] ]]
    geolocation [[
        description = [ Adds a map showing the locations of your contacts ]
    ]]
    gtkspell     [[ description = [ Enable spell checking using gtkspell ] ]]
    help         [[ description = [ Install the user documentation ] ]]
    ldap         [[ description = [ Enable LDAP support in evolution ] ]]
    spamassassin [[ description = [ Enable spam filtering using SpamAssassin ] ]]
    weather      [[ description = [ Build the weather calendar plugin ] ]]
    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia cs cy da de dz el
               en_AU en_CA en_GB en@shaw eo es et eu fa fi fr ga gl gu he hi hr hu id is it ja ka kk kn
               ko ku lt lv mai mk ml mn mr ms nb nds ne nl nn oc or pa pl ps pt pt_BR ro ru rw si sk
               sl sq sr sr@latin sv ta te tg th tr ug uk vi wa xh zh_CN zh_HK zh_TW )
"
# pst-import plugin needs libpst

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        dev-util/pkg-config
        gnome-desktop/yelp-tools
        gtk-doc? ( dev-doc/gtk-doc[>=1.14] )
    build+run:
        app-spell/enchant[>=1.1.7]
        app-text/iso-codes[>=0.49]
        dev-libs/atk
        dev-libs/glib:2[>=2.46.0]
        dev-libs/libcryptui
        dev-libs/libxml2:2.0[>=2.7.3]
        dev-libs/nspr
        dev-libs/nss
        gnome-desktop/gnome-autoar[>=0.1.1]
        gnome-desktop/evolution-data-server:1.2[>=$(ever range 1-3)][gtk][weather?][ldap?]
        gnome-desktop/gcr[>=3.4]
        gnome-desktop/gnome-desktop:3.0[>=2.91.3]
        gnome-desktop/gsettings-desktop-schemas[>=2.91.92]
        gnome-desktop/libsoup:2.4[>=2.42]
        media-libs/libcanberra[>=0.25][providers:gtk3]
        net-libs/webkit:4.0[>=2.13.90]
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0[>=2.24.0]
        x11-libs/gtk+:3[>=3.16.0]
        x11-misc/shared-mime-info[>=0.22]
        bogofilter? ( mail-filter/bogofilter )
        geolocation? (
            gnome-desktop/geocode-glib:1.0[>=3.10.0]
            x11-libs/clutter-gtk:1.0[>=0.90]
            x11-libs/libchamplain:0.12
        )
        gtkspell? ( app-spell/gtkspell:3.0 )
        ldap? ( net-directory/openldap[>=2.4.0] )
        libnotify? ( x11-libs/libnotify[>=0.7] )
        spamassassin? ( mail-filter/spamassassin )
        weather? ( gnome-desktop/libgweather:3.0[>=3.10] )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--enable-autoar'
    '--enable-canberra'
    '--enable-gnome-desktop'
    '--enable-libcryptui'
    '--enable-nls'
    '--enable-nss'
    '--enable-plugins=all'
    '--enable-smime'

    # not yet packaged
    '--disable-pst-import'
    '--disable-text-highlight'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'bogofilter'
    'geolocation contact-maps'
    'gtk-doc'
    'gtkspell'
    'spamassassin'
    'weather'
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    'help'
    'ldap openldap'
)

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

