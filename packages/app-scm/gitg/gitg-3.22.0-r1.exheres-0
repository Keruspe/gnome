# Copyright 2013 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gitg
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

PLATFORMS="~amd64"

MYOPTIONS="debug
    ( linguas:
        as bg bs ca ca@valencia cs da de el en_GB eo es eu fi fr gl he hu id it ja ko lt lv nb nl oc
        pa pl pt pt_BR ro ru sk sl sr sr@latin sv tr uk zh_CN zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40]
        sys-devel/gettext[>=0.17]
        sys-devel/libtool
    build+run:
        app-spell/gtkspell:3.0[>=3.0.3][vapi]
        base/libgee:0.8[gobject-introspection]
        dev-libs/glib:2[>=2.38]
        dev-libs/libpeas:1.0[>=1.5.0]
        dev-libs/libsecret:1
        dev-libs/libxml2:2.0[>=2.9.0]
        dev-scm/libgit2-glib:1.0[>=0.25.0&<0.26.0][vapi]
        gnome-desktop/gobject-introspection:1[>=0.10.1]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/gtksourceview:3.0[>=3.10]
        gnome-desktop/libsoup:2.4
        x11-libs/gtk+:3[>=3.20.0]
"

DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/libgit2-glib.patch )

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'debug' )
DEFAULT_SRC_CONFIGURE_PARAMS=(
    # needs unpackaged valadoc
    '--disable-docs'

    '--disable-python'
)

src_prepare() {
    edo sed -e '/GNOME_COMPILE_WARNINGS/d' -i configure.ac
    autotools_src_prepare
}
