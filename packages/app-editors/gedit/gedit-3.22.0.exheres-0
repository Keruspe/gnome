# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gedit vala [ with_opt=true option_name=vapi vala_dep=true ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc python spell
    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs cy da de dz
               el en_CA en_GB en@shaw eo es et eu fa fi fr ga gd gl gu he hi hr hu hy id is it ja ka
               kk km kn ko ku la lt lv mai mg mi mk ml mn mr ms my nb nds ne nl nn oc or pa pl ps pt
               pt_BR ro ru rw si sk sl sq sr sr@latin sv ta te tg th tk tr ug uk vi wa xh zh_CN zh_HK
               zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.1]
        dev-util/itstool [[ note = [ from YELP_HELP_INIT in configure.ac ] ]]
        virtual/pkg-config[>=0.20]
        sys-devel/gettext[>=0.18]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libpeas:1.0[>=1.14.1][python?]
        dev-libs/libxml2:2.0[>=2.5.0]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/gtksourceview:3.0[>=3.21.2][gobject-introspection]
        x11-libs/cairo
        x11-libs/gdk-pixbuf
        x11-libs/gtk+:3[>=3.21.3][gobject-introspection]
        x11-libs/libICE
        x11-libs/libSM[>=1.0.0]
        x11-libs/libX11
        x11-libs/pango
        gnome-desktop/gobject-introspection:1[>=0.9.3]
        python? ( dev-lang/python:=[>=3.2.3]
                  gnome-bindings/pygobject:3[>=3.0.0] )
        spell? ( gnome-desktop/gspell:1[>=0.2.5] )
    suggestion:
        gnome-desktop/gedit-plugins [[ description = [ Plugins for additional features ] ]]
"

RESTRICT="test" # uses X

DEFAULT_SRC_CONFIGURE_PARAMS=( --exec-prefix=/usr/$(exhost --target) '--disable-updater' '--enable-introspection' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( gtk-doc python spell 'vapi vala' )

AT_M4DIR=( libgd )

src_prepare() {
    # NOTE(compnerd) avoid the dependency on gnome-common
    edo sed -e 's/GNOME_COMPILE_WARNINGS/dnl &/' \
            -i "${WORK}/configure.ac"
    edo sed -e 's/@WARN_CFLAGS@//' \
            -i $(find "${WORK}" -name Makefile.am)
    edo intltoolize --force --automake
    autotools_src_prepare
}

src_configure() {
    if optionq python ; then
        default
    else
        am_cv_pathless_PYTHON=true default
    fi
}

